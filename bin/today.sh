#!/bin/bash

FILEDIR=$HOME/.today
TODAY=$(date +"%Y-%m-%d")
TODAYS_FILE=$FILEDIR/today-$TODAY.md
FRIENDS=(@brody @josh @madams @russ)

if [ ! -d "${FILEDIR}" ]; then
  mkdir -p $FILEDIR
fi

if [ ! -f "${TODAYS_FILE}" ]; then
  cat << EOT > ${TODAYS_FILE}
# $TODAY

## Standup
EOT

  for friend in ${FRIENDS[@]}; do
    cat << EOT >> ${TODAYS_FILE}

### $friend
* y
    - 
* t
    - 
EOT
  done
  cat << EOT >> ${TODAYS_FILE}
    
## WTF I did today
EOT

fi

nvim ${TODAYS_FILE}
